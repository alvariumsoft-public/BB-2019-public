require('./bootstrap');

require('./libs');

require('./post-pagination');

require('./news-pagination')

import Vue from 'vue';

import Team from "./components/Team";


new Vue({
    el: '#app',
    components:{
        'team': Team,
    }
});
