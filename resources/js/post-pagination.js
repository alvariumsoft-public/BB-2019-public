"use strict";

document.addEventListener('DOMContentLoaded', function () {

    var button = document.querySelector("#comment-button");
    if (button) {
        button.addEventListener('click', function(e){
            var target = e.target;
            var url = target.getAttribute('data-url');
            var dataOffset = parseInt(target.getAttribute('data-offset'));
            e.preventDefault();
            var count = parseInt(target.getAttribute('data-count'));
            var formData = new FormData();
            var locale = target.getAttribute('data-locale');
            formData.append('dataOffset', dataOffset);
            formData.append('locale', locale);
            var xhr = new XMLHttpRequest();
            xhr.open("POST", url);
            xhr.send(formData);
            xhr.addEventListener('readystatechange', function(e)
            {
                if (this.readyState == 4 && this.status == 200)
                {
                    if (this.responseText) {
                        var moreAds = document.querySelector("#moreAds");
                        moreAds.innerHTML = moreAds.innerHTML+this.responseText;
                        dataOffset += 3;
                        button.setAttribute('data-offset', dataOffset);
                        if (dataOffset >= count) {
                            button.style.display = 'none';
                        }
                    }
                    else {
                        button.style.display = 'none';
                    }
                };
            });
        });
    }
});
