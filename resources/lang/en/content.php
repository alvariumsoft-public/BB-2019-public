<?php
return [
    //home page
    'main_title1' => "EBRD GPA",
    'main_title2' => "Technical Cooperation Facility",
    'about' => 'About',
    'download_can' => 'You can download this document for review',
    'document_title' => "The EBRD GPA Technical \nCooperation Facility Factsheet",
    'download_button' => 'Download',
    'copyright' => '© 2020 European Bank for Reconstruction and Development',
    'site_by' => 'Site by XXX.',
    'disigned' => 'Designed by Red Rocket',
    'about_first_title' => 'European Bank',
    'about_first_description' => 'for Reconstruction and Development',
    'wto' => 'World Trade Organization',
    //menu
    'menu' => 'Menu',
    'menu_about' => 'ABOUT',
    'gpa' => 'GPA',
    'technical_cooperation' => 'Technical cooperation',
    'assessment' => 'Assessments',
    'news' => 'News',
    'resources' => 'Resources',
    'contact' => 'Contact us',
    'terms' => 'Terms and conditions',
    'privacy' => 'Privacy Policy',
    'taskforce' => 'Taskforce',


    //assessment page
    'download_description' => 'Though the reports show overall some general compliance aspects, it focuses mostly on encountered regulatory gaps between the GPA and the national legislation. For the purpose of this analysis, such discrepancies have been identified through the assessment based on the above-mentioned EBRD compliance questionnaire. The employed questionnaire is not to be confused with the WTO’s own Checklist of Issues for provision of information relating to accession to the revised GPA.',
    'download_title' => "GPA Compliance Assessments",
    'read_button' => 'Read more',
    'assessments' => 'Assessments',
    //assessment??

    //contact page
    'first_name' => 'First name',
    'email_address' => 'Email Address',
    'your_enquiry' => 'Your enquiry',
    'send' => 'Send',

    //gpa agreement page
    'gpa_title1' => "Agreement on",
    'gpa_title2' => "Government Procurement",


    //terms
    'terms&' => 'Terms & conditions',

    //technical cooperation
    'gpa_patries' => 'GPA Patries',
    'gpa_observers' => 'GPA Observers',

    //resources
    'view' => 'View',

    //mail
    'mail_sent' => 'Mail sent',
    'name' => 'Name',
    'email' => 'Email',
    'enquiry' => 'Enquiry',
    'new_contact' => 'New Enquiry',
    'mail_error' => 'Something went wrong'
];
