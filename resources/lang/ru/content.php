<?php
return [
    //home page
    'main_title1' => "EBRD GPA",
    'main_title2' => "Technical Cooperation Facility",
    'about' => 'About',
    'download_can' => 'You can download this document for review',
    'document_title' => "The EBRD GPA Technical \nCooperation Facility Factsheet",
    'download_button' => 'Download',
    'copyright' => '© 2020 European Bank for Reconstruction and Development',
    'site_by' => 'Site by XXX.',
    'disigned' => 'Designed by Red Rocket',
    'about_first_title' => 'European Bank',
    'about_first_description' => 'for Reconstruction and Development',
    'wto' => 'World Trade Organization',
    //menu
    'menu' => 'Меню',
    'menu_about' => 'О нас',
    'gpa' => 'GPA',
    'technical_cooperation' => 'Техническое сотрудничество',
    'assessment' => 'Оценка',
    'news' => 'Новости',
    'resources' => 'Ресурсы',
    'contact' => 'Связаться с нами',
    'terms' => 'Условия и положения',
    'privacy' => 'Политика конфиденциальности',
    'taskforce' => 'Рабочая группа',


    //assessment page
    'download_description' => 'Though the reports show overall some general compliance aspects, it focuses mostly on encountered regulatory gaps between the GPA and the national legislation. For the purpose of this analysis, such discrepancies have been identified through the assessment based on the above-mentioned EBRD compliance questionnaire. The employed questionnaire is not to be confused with the WTO’s own Checklist of Issues for provision of information relating to accession to the revised GPA.',
    'download_title' => "GPA Compliance Assessments",
    'read_button' => 'Read more',
    'assessments' => 'Assessments',
    //assessment??

    //contact page
    'first_name' => 'First name',
    'email_address' => 'Email Address',
    'your_enquiry' => 'Your enquiry',
    'send' => 'Send',

    //gpa agreement page
    'gpa_title1' => "Agreement on",
    'gpa_title2' => "Government Procurement",


    //terms
    'terms&' => 'Terms & conditions',

    //technical cooperation
    'gpa_patries' => 'GPA Patries',
    'gpa_observers' => 'GPA Observers',

    //resources
    'view' => 'View',

    //mail
    'mail_sent' => 'Письмо отправлено',
    'name' => 'Имя',
    'email' => 'Email',
    'enquiry' => 'Запрос',
    'new_contact' => 'Новый Запрос',
    'mail_error' => 'Что-то пошло не так'
];
