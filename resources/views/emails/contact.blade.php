@component('mail::message')

<p>{{ __('content.name') }}: {{ $data['name'] }}</p>
<p>{{ __('content.email') }}: {{ $data['email'] }}</p>
<p>{{ __('content.enquiry') }}: {{ $data['enquiry'] }}</p>

@component('mail::button', ['url' => route('home')])
To site {{ env('APP_NAME') }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
