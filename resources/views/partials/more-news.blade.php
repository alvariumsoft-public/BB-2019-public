@foreach($news as $item)
    <div class="card-item">
        <div class="card-item__img">
            <img src="{{ asset($item->image) }}" alt="">
        </div>
        <div class="card-item__content">
            <div class="card-item__date">{{ $item->getTranslation($locale)->date }}</div>
            <h3 class="card-item__title">{{ $item->getTranslation($locale)->title }}</h3>
            <p>
                {!! $item->getTranslation($locale)->description !!}
            </p>
            <div class="card-item__bottom">
                <a href="{{ $item->link }}"><img src="{{ asset('/storage/img/arrow-right-long.svg') }}" alt=""></a>
            </div>
        </div>
    </div>
@endforeach
