@foreach($posts as $item)
    <div class="card-item">
        <div class="card-item__img">
            <img src="{{ asset($item->image) }}" alt="">
        </div>
        <div class="card-item__content">
            <h3 class="card-item__title">{{ $item->getTranslation($locale)->title }}</h3>
            <p>
                {!! $item->getTranslation($locale)->short_description !!}
            </p>
            <div class="card-item__bottom">
                <a href="{{ route('post', $item->slug) }}"><img src="{{ asset('/storage/img/arrow-right-long.svg') }}" alt=""></a>
            </div>
        </div>
    </div>
@endforeach

