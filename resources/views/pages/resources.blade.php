@extends('layouts.app')

@section('content')
    <section class="s-top" style="background-image: url({{ asset($content->image) }});">
        <div class="container">
            <h1 class="main-title">{{ __('content.resources') }}</h1>
        </div>
    </section>

    <section class="s-resources">
        <div class="container">
            <div class="blue-text">
                <p>
                    {{ $content->title }}
                </p>
            </div>
            <div class="rs-wrap">
                <div class="rs-wrap__top-text">{{ __('content.resources') }}</div>
                @foreach($documents as $document)
                    <div class="rs-item">
                        <div class="rs-item__left">
                            <div class="rs-item__icon">
                                <img src="{{ asset('/storage/img/file-icon.svg') }}" alt="">
                            </div>
                            <h3 class="rs-item__title">{{ $document->title }}</h3>
                        </div>
                        <div class="rs-item__right">
                            <a target="_blank" href="{{ asset($document->document) }}" class="btn btn-transparent btn-blue">{{ __('view') }}</a>
                            <a download="{{ $document->document }}" href="{{ asset($document->document) }}" class="btn btn-transparent btn-blue">{{ __('content.download_button') }}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
