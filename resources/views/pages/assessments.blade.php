@extends('layouts.app')

@section('content')
    <section class="s-top s-top-assessment" style="background-image: url({{ asset($content->image) }});">
        <div class="container">
            <h1 class="main-title">{{ __('content.assessments') }}</h1>
        </div>
    </section>

    <section class="s-as">
        <div class="container">
            <div class="blue-text">
                <p>
                    {{ $content->title }}
                </p>
            </div>
            <div class="text-two-col">
                <div class="text-two-col__left">
                    {!! $content->first_block !!}
                </div>
                <div class="text-two-col__right">
                    {!! $content->second_block !!}
                </div>
            </div>
        </div>
    </section>

    <section class="s-as-bottom">
        <div class="container">
            <p>
                {{ __('content.download_description') }}
            </p>
            <div class="as-bottom-info">
                <div class="as-bottom-info__icon">
                    <img src="{{ asset('/storage/img/assessment-icon.svg') }}" alt="">
                </div>
                <div class="as-bottom-info__text">
                    <h2>{{ __('content.download_title') }}</h2>
                    <a href="{{ route('resources') }}" class="btn btn-transparent">{{ __('content.read_button') }}</a>
                </div>
            </div>
        </div>
    </section>
@endsection
