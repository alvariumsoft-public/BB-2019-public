@extends('layouts.app')

@section('content')
    <section class="s-top s-top-map" style="background-image: url({{ asset($content->image) }});">
        <div class="container">
            <div class="locations">
                <div class="location-item">
                    <img src="{{ asset('/storage/img/location-icon-1.svg') }}" alt="">
                    <span>{{ __('content.gpa_patries') }}</span>
                </div>
                <div class="location-item">
                    <img src="{{ asset('/storage/img/location-icon-2.svg') }}" alt="">
                    <span>{{ __('content.gpa_observers') }}</span>
                </div>
            </div>
        </div>
    </section>

    <section class="s-building">
        <div class="container">
            <h1 class="main-title blue">{{ $content->title }}</h1>
            <div class="blue-text">
                <p>
                    {!! $content->description !!}
                </p>
            </div>
            <div class="text-two-col">
                <div class="text-two-col__left">
                {!! $content->first_block !!}
                </div>
                <div class="text-two-col__right">
                {!! $content->second_block !!}
                </div>
            </div>
        </div>
    </section>

    <section class="s-technical">
        <div class="s-technical__bg"></div>
        <div class="container">
            <div id="moreAds" class="card-items">
                @foreach($posts as $item)
                    <div class="card-item">
                        <div class="card-item__img">
                            <img src="{{ asset($item->image) }}" alt="">
                        </div>
                        <div class="card-item__content">
                            <h3 class="card-item__title">{{ $item->title }}</h3>
                            <p>
                                {{ $item->short_description }}
                            </p>
                            <div class="card-item__bottom">
                                <a href="{{ route('post', $item->slug) }}"><img src="{{ asset('/storage/img/arrow-right-long.svg') }}" alt=""></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @if($count > $dataOffset)
                <div class="s-technical__btn">
                    <a id="comment-button" class="btn btn-transparent btn-blue"
                       data-count="{{ $count }}"
                       data-locale="{{\Illuminate\Support\Facades\App::getLocale()}}"
                       data-offset="{{ $dataOffset }}" data-url="{{ route('morePosts') }}">{{ __('content.read_button') }}</a>
                </div>
            @endif
        </div>
    </section>
@endsection
