@extends('layouts.app')

@section('content')
    <section class="s-top" style="background-image: url({{ asset($content->image) }});">
        <div class="container">
            <h1 class="main-title">{{ __('content.news') }}</h1>
        </div>
    </section>

    <section class="s-news">
        <div class="container">
            <div id="news" class="card-items">
                @foreach($news as $item)
                    <div class="card-item">
                        <div class="card-item__img">
                            <img src="{{ asset($item->image) }}" alt="">
                        </div>
                        <div class="card-item__content">
                            <div class="card-item__date">{{ $item->date }}</div>
                            <h3 class="card-item__title">{{ $item->title }}</h3>
                                {!! $item->description !!}
                            <div class="card-item__bottom">
                                <a target="_blank" href="{{ $item->link }}"><img src="{{ asset('/storage/img/arrow-right-long.svg') }}" alt=""></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @if($count > $dataOffset)
                <div class="s-news__btn">
                    <a id="news-button" class="btn btn-transparent btn-blue"
                       data-count="{{ $count }}"
                       data-locale="{{\Illuminate\Support\Facades\App::getLocale()}}"
                       data-offset="{{ $dataOffset }}" data-url="{{ route('moreNews') }}">{{ __('content.read_button') }}</a>
                </div>
            @endif
        </div>
    </section>
@endsection
