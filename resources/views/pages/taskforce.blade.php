@extends('layouts.app')

@section('content')
    <section class="s-top" style="background-image: url({{ asset($content->image) }});">
        <div class="container">
            <h1 class="main-title blue shadow">{{ __('content.taskforce') }}</h1>
        </div>
    </section>

    <section class="s-taskforce-text">
        <div class="container">
            <div class="blue-text">
                <p>
                    {{ $content->title }}
                </p>
            </div>
        </div>
    </section>

    <section class="s-taskforce">
        <div class="container">

{{--                @foreach($members as $member)--}}
                <team :team="{{ $members }}"></team>
{{--                    @include('partials.team-member', ['member' => $member])--}}
{{--                @endforeach--}}
            </div>

    </section>

    <div class="popup-overlay"></div>
@endsection
{{--<script>--}}
{{--    import Team from "../../js/components/Team";--}}
{{--    export default {--}}
{{--        components: {Team}--}}
{{--    }--}}
{{--</script>--}}
