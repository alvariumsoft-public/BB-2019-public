@extends('layouts.app')

@section('content')
    <section class="s-top s-top-gpa" style="background-image: url({{ asset($content->image) }});">
        <div class="container">
            <h1 class="main-title">{{ __('content.gpa_title1') }} <br> {{ __('content.gpa_title2') }}</h1>
        </div>
        <a href="#s-accordion" class="s-top-arrow"><img src="{{ asset('/storage/img/down-arrow-icon.svg') }}" alt=""></a>
    </section>

    <section class="s-accordion" id="s-accordion">
        <div class="container">
            <div class="blue-text">
                <p>
                    {{ $content->title }}
                </p>
            </div>
            <div class="accordion-wrap">
                @foreach($blocks as $block)
                <div class="accordion-item">
                    <div class="accordion-title">
                        <h4 class="accordion-title__text">{{ $block->title }}</h4>
                        <div class="accordion-arrow"><img src="{{ asset('/storage/img/down-arrow-long.svg') }}" alt=""></div>
                    </div>
                    <div class="accordion-content">
                        <div class="accordion-content__inner">
                            <div class="accordion-content__left">
                                {!! $block->first_block !!}
                            </div>
                            <div class="accordion-content__right">
                                {!! $block->second_block !!}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
