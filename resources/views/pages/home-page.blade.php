@extends('layouts.app')

@section('content')
<section class="s-top-intro" style="background-image: url({{ asset($content->image) }});">
	<div class="container">
		<h1 class="main-title">{{ __('content.main_title1') }} <br> {{ __('content.main_title2') }}</h1>
	</div>
	<div class="s-top-intro__bottom">
		<div class="container-fluid">
			<div class="s-top-intro__bottom-img">
				<img src="{{ asset('/storage/img/home-top-icon-1.svg') }}" alt="">
			</div>
			<div class="s-top-intro__bottom-img">
				<img src="{{ asset('/storage/img/home-top-icon-2.svg') }}" alt="">
			</div>
		</div>
	</div>
    <a href="#s-text-intro" class="s-top-arrow"><img src="{{ asset('/storage/img/down-arrow-icon.svg') }}" alt=""></a>
</section>

<section class="s-text-intro" id="s-text-intro">
	<div class="container">
		<div class="blue-text">
			<p>
				{{ $content->title }}
			</p>
		</div>
		<div class="text-two-col">
			<div class="text-two-col__left">
                 {!! $content->first_block !!}
			</div>
			<div class="text-two-col__right">
                {!! $content->second_block !!}
			</div>
		</div>
	</div>
</section>

<section class="s-about">
	<div class="container">
		<h2 class="s-title">{{ __('content.about') }}</h2>
	</div>
</section>

<div class="container">
	<div class="about-items">
		<div class="about-item">
			<div class="about-item__img">
				<img src="{{ asset('/storage/img/about-img-1.jpg') }}" alt="">
			</div>
			<div class="about-item__content">
				<div class="about-item__logo">
					<img src="{{ asset('/storage/img/about-logo-1.svg') }}" alt="">
				</div>
                {!! $content->about_first_block_text !!}
				<div class="about-item__bottom">
					<span>{{ $content->about_first_link_info }}</span>
					<a target="_blank" href="{{ $content->about_first_link }}"><img src="{{ asset('/storage/img/arrow-right-long.svg') }}" alt=""></a>
				</div>
			</div>
		</div>
		<div class="about-item">
			<div class="about-item__img">
				<img src="{{ asset('/storage/img/about-img-2.jpg') }}" alt="">
			</div>
			<div class="about-item__content">
				<div class="about-item__logo">
					<img src="{{ asset('/storage/img/about-logo-2.svg') }}" alt="">
				</div>
                {!! $content->about_second_block_text !!}
				<div class="about-item__bottom">
					<span>{{ $content->about_second_link_info }}</span>
					<a target="_blank" href="{{ $content->about_second_link }}"><img src="{{ asset('/storage/img/arrow-right-long.svg') }}" alt=""></a>
				</div>
			</div>
		</div>
	</div>
</div>


<section class="s-download">
	<div class="download-card">
		<div class="download-card__icon">
			<img src="{{ asset('/storage/img/download-icon.svg') }}" alt="">
		</div>
		<div class="download-card__text">
			<p>{{ __('content.download_can') }}</p>
			<h2>{{ __('content.document_title') }}</h2>
			<a download="13387 EBRD (GPA Facility Factsheet ARTWORK)" href="{{ asset('storage/docs/13387 EBRD (GPA Facility Factsheet ARTWORK).pdf') }}" class="btn btn-transparent">{{ __('content.download_button') }}</a>
		</div>
	</div>
</section>
@endsection

