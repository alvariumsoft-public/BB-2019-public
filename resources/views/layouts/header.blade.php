@php($name = \Illuminate\Support\Facades\Route::currentRouteName())
<header class="top-header {{ ($name == 'home') ? 'header-home' : '' }}">
    <div class="container-fluid">
        <div class="top-header__wrap">
            <div class="top-header__left">
                <a href="{{ route('home') }}" class="top-header__logo">
                    <img src="{{ asset('/storage/img/header-logo-1.svg') }}" alt="">
                    <img src="{{ asset('/storage/img/header-logo-2.svg') }}" alt="">
                </a>
                <div class="hamburger-wrap">
                    <span>{{ __('content.menu') }}</span>
                    <div class="hamburger hamburger--slider">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-header__right">
                <ul class="lang-items">
                    <li class="lang-item {{app()->getLocale() == 'en' ? 'active' : ''}}"><a href="{{ route('setLocale', 'en') }}">EN</a></li>
                    <li class="lang-item {{app()->getLocale() == 'ru' ? 'active' : ''}}"><a href="{{ route('setLocale', 'ru') }}">RU</a></li>
                    <li class="lang-item {{app()->getLocale() == 'es' ? 'active' : ''}}"><a href="{{ route('setLocale', 'es') }}">ES</a></li>
                    <li class="lang-item {{app()->getLocale() == 'fr' ? 'active' : ''}}"><a href="{{ route('setLocale', 'fr') }}">FR</a></li>
                </ul>
                <nav class="main-menu">
                    <ul>
                        <li class="{{ ($name == 'about') ? 'active' : '' }}"><a href="{{ route('home') }}">{{ __('content.menu_about') }}</a></li>
                        <li class="{{ ($name == 'gpa') ? 'active' : '' }}"><a href=" {{ route('gpa') }}">{{ __('content.gpa') }}</a></li>
                        <li class="{{ ($name == 'cooperation') ? 'active' : '' }}">
                            <div class="main-menu__caret-group">
                                <a href="{{ route('cooperation') }}">{{ __('content.technical_cooperation') }}</a>
                                <span class="main-menu__caret"><img src="{{ asset('/storage/img/caret-down.svg') }}" alt=""></span>
                            </div>
                            <ul class="sub-menu">
                                <li><a href="{{ route('taskforce') }}">{{ __('content.taskforce') }}</a></li>
                            </ul>
                        </li>
                        <li class="{{ ($name == 'assessments') ? 'active' : '' }}"><a href="{{ route('assessments') }}">{{ __('content.assessment') }}</a></li>
                        <li class="{{ ($name == 'news') ? 'active' : '' }}"><a href="{{ route('news') }}">{{ __('content.news') }}</a></li>
                        <li class="{{ ($name == 'resources') ? 'active' : '' }}"><a href="{{ route('resources') }}">{{ __('content.resources') }}</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
