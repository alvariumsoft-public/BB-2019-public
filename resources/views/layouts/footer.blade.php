<footer class="footer">
    <div class="footer__top">
        <div class="container">
            <div class="footer-logo">
                <div class="footer-logo__item">
                    <img src="{{ asset('/storage/img/footer-logo-1.svg') }}" alt="">
                </div>
                <div class="footer-logo__item">
                    <img src="{{ asset('/storage/img/footer-logo-2.svg') }}" alt="">
                </div>
            </div>
            <nav class="footer-nav">
                <ul>
                    <li><a href="{{ route('contacts') }}">{{ __('content.contact') }}</a></li>
                    <li><a href="{{ route('terms') }}">{{ __('content.terms') }}</a></li>
                    <li><a href="{{ route('privacy') }}">{{ __('content.privacy') }}</a></li>
                </ul>
            </nav>
            <div class="copyright">{{ __('content.copyright') }}</div>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="container">
            <div class="footer-info"><span>{{ __('content.disigned') }}</span> <span>Developed by <a href="https://alvariumsoft.com/" target="_blank">Alvariumsoft</a></span></div>

        </div>
    </div>
</footer>
