<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <title>{{ env('APP_NAME') }}</title>
    @if( env('APP_ENV') != 'production')
    <meta name="robots" content="noindex">
    @endif
    <meta name="description" content="">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="mask-icon" href="{{ asset('/storage/img/footer-logo-1.svg') }}" color="grey">
    <link rel="shortcut icon" href="{{ asset('/storage/img/footer-logo-1.svg') }}" type="image/x-icon">
</head>

<body>
    <div id="app">
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
    </div>
<script src="{{ asset('/js/app.js') }}"></script>
</body>

</html>
