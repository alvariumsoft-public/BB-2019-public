<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <ul class="header__top--lang">
            <li><a href="{{ route('setLocale', 'en') }}" class="{{app()->getLocale() == 'en' ? 'active' : ''}}">EN</a></li>
            <li><a href="{{ route('setLocale', 'fr') }}" class="{{app()->getLocale() == 'fr' ? 'active' : ''}}">FR</a></li>
            <li><a href="{{ route('setLocale', 'ru') }}" class="{{app()->getLocale() == 'ru' ? 'active' : ''}}">RU</a></li>
            <li><a href="{{ route('setLocale', 'sp') }}" class="{{app()->getLocale() == 'sp' ? 'active' : ''}}">SP</a></li>
        </ul>
    @dd($content->title)
    </body>
</html>
