<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('page') }}'><i class='nav-icon la la-question'></i> Pages</a></li>--}}


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('homepage/1/edit') }}'><i class='nav-icon la la-house-damage'></i>Home Page</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('news') }}'><i class='nav-icon la la-list'></i>News</a></li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-group"></i>Taskforce</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('taskforcepage/1/edit') }}'><i class='nav-icon la la-file-text'></i>Page</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('teammember') }}'><i class='nav-icon la la-user'></i>Team</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-files-o"></i>Gpa Agreement</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('gpaagreementpage/1/edit') }}'><i class='nav-icon la la-file-text'></i>Page</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('gpaagreementcontent') }}'><i class='nav-icon la la-list'></i>Blocks</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-object-group"></i>Technical Operations</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('technicaloperation/1/edit') }}'><i class='nav-icon la la-file-text'></i>Page</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('post') }}'><i class='nav-icon la la-list'></i>Posts</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-newspaper"></i>Resources</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('resourcespage/1/edit') }}'><i class='nav-icon la la-file-text'></i>Page</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('documentresources') }}'><i class='nav-icon la la-list'></i>Documents</a></li>
    </ul>
</li>


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('assessmentspage/1/edit') }}'><i class='nav-icon la la-file'></i>Assessments</a></li>

{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('contactpage') }}'><i class='nav-icon la la-envelope-open-text'></i> Contact us</a></li>--}}

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('termspage/1/edit') }}'><i class='nav-icon la la-table'></i>Terms & Conditions</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('privacypolicypage/1/edit') }}'><i class='nav-icon la la-user-secret'></i>Privacy Policy</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('backup') }}'><i class='nav-icon la la-hdd-o'></i>Backups</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('log') }}'><i class='nav-icon la la-terminal'></i>Logs</a></li>
