<input type="hidden" name="{{ $column['name'] }}">

<div id="app">
    <survey-results :results="{{ $entry[$column['name']] }}" :template="{{$entry['survey_template_id']}}"></survey-results>
</div>

    <script src="{{ asset('js/app.js') }}" defer></script>


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style media="screen">
      .sv-panel__content {
        margin-left: 0px;
      }
    </style>
