<?php $languages = \App\Facades\LangActionsService::getListLanguages(); ?>
@include('crud::fields.inc.wrapper_start')
    <label>{{ $field['label'] }}</label>
    <select class="form-control" id="select_language">
    	@if (count($languages))
    		@foreach ($languages as $lang)
    			<option value="{{ $lang }}"
					@if (isset($field['value']) && $lang==$field['value'])
						 selected
					@endif
    			>{{ $lang }}</option>
    		@endforeach
    	@endif
	</select>
@include('crud::fields.inc.wrapper_end')


    @push('crud_fields_scripts')
        <script>
            function redirect_to_new_page_with_language_parameter() {
                var new_language = $("#select_language").val();
                var current_url = "{{ Request::url() }}";
                window.location.href = strip_last_language_parameter(current_url)+'?language='+new_language;
            }
            function strip_last_language_parameter(url) {
                if (url.indexOf("/create/") > -1 || url.indexOf("/edit/") > -1) {
                    var url_array = url.split('/');
                    url_array = url_array.slice(0, -1);
                    var clean_url = url_array.join('/');
                    return clean_url;
                }
                return url;
            }
            jQuery(document).ready(function($) {
                $("#select_language").change(function(e) {
                    var select_language_confirmation = confirm("Change language?");
                    if (select_language_confirmation == true) {
                        redirect_to_new_page_with_language_parameter();
                    } else {
                        // txt = "You pressed Cancel!";
                    }
                });

            });
        </script>
    @endpush

