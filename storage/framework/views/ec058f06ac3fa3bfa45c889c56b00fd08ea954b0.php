<?php $__env->startSection('content'); ?>
    <section class="s-top s-top-map" style="background-image: url(<?php echo e(asset($content->image)); ?>);">
        <div class="container">
            <div class="locations">
                <div class="location-item">
                    <img src="<?php echo e(asset('/storage/img/location-icon-1.svg')); ?>" alt="">
                    <span><?php echo e(__('content.gpa_patries')); ?></span>
                </div>
                <div class="location-item">
                    <img src="<?php echo e(asset('/storage/img/location-icon-2.svg')); ?>" alt="">
                    <span><?php echo e(__('content.gpa_observers')); ?></span>
                </div>
            </div>
        </div>
    </section>

    <section class="s-building">
        <div class="container">
            <h1 class="main-title blue"><?php echo e($content->title); ?></h1>
            <div class="blue-text">
                <p>
                    <?php echo $content->description; ?>

                </p>
            </div>
            <div class="text-two-col">
                <div class="text-two-col__left">
                <?php echo $content->first_block; ?>

                </div>
                <div class="text-two-col__right">
                <?php echo $content->second_block; ?>

                </div>
            </div>
        </div>
    </section>

    <section class="s-technical">
        <div class="s-technical__bg"></div>
        <div class="container">
            <div id="moreAds" class="card-items">
                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="card-item">
                        <div class="card-item__img">
                            <img src="<?php echo e(asset($item->image)); ?>" alt="">
                        </div>
                        <div class="card-item__content">
                            <h3 class="card-item__title"><?php echo e($item->title); ?></h3>
                            <p>
                                <?php echo e($item->short_description); ?>

                            </p>
                            <div class="card-item__bottom">
                                <a href="<?php echo e(route('post', $item->slug)); ?>"><img src="<?php echo e(asset('/storage/img/arrow-right-long.svg')); ?>" alt=""></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <?php if($count > $dataOffset): ?>
                <div class="s-technical__btn">
                    <a id="comment-button" class="btn btn-transparent btn-blue"
                       data-count="<?php echo e($count); ?>"
                       data-offset="<?php echo e($dataOffset); ?>" data-url="<?php echo e(route('morePosts')); ?>"><?php echo e(__('content.read_button')); ?></a>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/alvariumsoft.dev/data/www/bb2019.alvariumsoft.dev/resources/views/pages/tech-cooperation.blade.php ENDPATH**/ ?>