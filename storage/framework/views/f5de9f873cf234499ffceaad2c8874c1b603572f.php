<?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="card-item">
        <div class="card-item__img">
            <img src="<?php echo e(asset($item->image)); ?>" alt="">
        </div>
        <div class="card-item__content">
            <h3 class="card-item__title"><?php echo e($item->title); ?></h3>
            <p>
                <?php echo e($item->short_description); ?>

            </p>
            <div class="card-item__bottom">
                <a href="#"><img src="<?php echo e(asset('/storage/img/arrow-right-long.svg')); ?>" alt=""></a>
            </div>
        </div>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php /**PATH /var/www/alvariumsoft.dev/data/www/bb2019.alvariumsoft.dev/resources/views/partials/more-posts.blade.php ENDPATH**/ ?>