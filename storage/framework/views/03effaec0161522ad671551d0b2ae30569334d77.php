<?php ($name = \Illuminate\Support\Facades\Route::currentRouteName()); ?>
<header class="top-header <?php echo e(($name == 'home') ? 'header-home' : ''); ?>">
    <div class="container-fluid">
        <div class="top-header__wrap">
            <div class="top-header__left">
                <div class="top-header__logo">
                    <img src="<?php echo e(asset('/storage/img/header-logo-1.svg')); ?>" alt="">
                    <img src="<?php echo e(asset('/storage/img/header-logo-2.svg')); ?>" alt="">
                </div>
                <div class="hamburger-wrap">
                    <span><?php echo e(__('content.menu')); ?></span>
                    <div class="hamburger hamburger--slider">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-header__right">
                <ul class="lang-items">
                    <li class="lang-item <?php echo e(app()->getLocale() == 'en' ? 'active' : ''); ?>"><a href="<?php echo e(route('setLocale', 'en')); ?>">EN</a></li>
                    <li class="lang-item <?php echo e(app()->getLocale() == 'ru' ? 'active' : ''); ?>"><a href="<?php echo e(route('setLocale', 'ru')); ?>">RU</a></li>
                    <li class="lang-item <?php echo e(app()->getLocale() == 'sp' ? 'active' : ''); ?>"><a href="<?php echo e(route('setLocale', 'sp')); ?>">SP</a></li>
                    <li class="lang-item <?php echo e(app()->getLocale() == 'fr' ? 'active' : ''); ?>"><a href="<?php echo e(route('setLocale', 'fr')); ?>">FR</a></li>
                </ul>
                <nav class="main-menu">
                    <ul>
                        <li class="<?php echo e(($name == 'about') ? 'active' : ''); ?>"><a href="#"><?php echo e(__('content.about')); ?></a></li>
                        <li class="<?php echo e(($name == 'gpa') ? 'active' : ''); ?>"><a href=" <?php echo e(route('gpa')); ?>"><?php echo e(__('content.gpa')); ?></a></li>
                        <li class="<?php echo e(($name == 'cooperation') ? 'active' : ''); ?>">
                            <div class="main-menu__caret-group">
                                <a href="<?php echo e(route('cooperation')); ?>"><?php echo e(__('content.technical_cooperation')); ?></a>
                                <span class="main-menu__caret"><img src="<?php echo e(asset('/storage/img/caret-down.svg')); ?>" alt=""></span>
                            </div>
                            <ul class="sub-menu">
                                <li><a href="#"><?php echo e(__('content.taskforce')); ?></a></li>
                            </ul>
                        </li>
                        <li><a href="#"><?php echo e(__('content.assessment')); ?></a></li>
                        <li><a href="#"><?php echo e(__('content.news')); ?></a></li>
                        <li><a href="#"><?php echo e(__('content.resources')); ?></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<?php /**PATH /var/www/alvariumsoft.dev/data/www/bb2019.alvariumsoft.dev/resources/views/layouts/header.blade.php ENDPATH**/ ?>