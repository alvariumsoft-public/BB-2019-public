<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>

    <meta charset="utf-8">
    <title><?php echo e(env('APP_NAME')); ?></title>
    <meta name="robots" content="noindex">
    <meta name="description" content="">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

    <link rel="stylesheet" href="<?php echo e(asset('/css/app.css')); ?>">

</head>

<body>

<?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent('content'); ?>
<?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script src="<?php echo e(asset('/js/app.js')); ?>"></script>
</body>

</html>
<?php /**PATH /var/www/alvariumsoft.dev/data/www/bb2019.alvariumsoft.dev/resources/views/layouts/app.blade.php ENDPATH**/ ?>