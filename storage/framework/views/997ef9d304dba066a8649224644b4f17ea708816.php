<?php $__env->startSection('content'); ?>
<section class="s-top-intro" style="background-image: url(<?php echo e(asset($content->image)); ?>);">
	<div class="container">
		<h1 class="main-title"><?php echo e(__('content.main_title1')); ?> <br> <?php echo e(__('content.main_title2')); ?></h1>
	</div>
	<div class="s-top-intro__bottom">
		<div class="container-fluid">
			<div class="s-top-intro__bottom-img">
				<img src="<?php echo e(asset('/storage/img/home-top-icon-1.svg')); ?>" alt="">
			</div>
			<div class="s-top-intro__bottom-img">
				<img src="<?php echo e(asset('/storage/img/home-top-icon-2.svg')); ?>" alt="">
			</div>
		</div>
	</div>
    <a href="#s-text-intro" class="s-top-arrow"><img src="<?php echo e(asset('/storage/img/down-arrow-icon.svg')); ?>" alt=""></a>
</section>

<section class="s-text-intro" id="s-text-intro">
	<div class="container">
		<div class="blue-text">
			<p>
				<?php echo e($content->title); ?>

			</p>
		</div>
		<div class="text-two-col">
			<div class="text-two-col__left">
                 <?php echo $content->first_block; ?>

			</div>
			<div class="text-two-col__right">
                <?php echo $content->second_block; ?>

			</div>
		</div>
	</div>
</section>

<section class="s-about">
	<div class="container">
		<h2 class="s-title"><?php echo e(__('content.about')); ?></h2>
	</div>
</section>

<div class="container">
	<div class="about-items">
		<div class="about-item">
			<div class="about-item__img">
				<img src="<?php echo e(asset('/storage/img/about-img-1.jpg')); ?>" alt="">
			</div>
			<div class="about-item__content">
				<div class="about-item__logo">
					<img src="<?php echo e(asset('/storage/img/about-logo-1.svg')); ?>" alt="">
				</div>
                <?php echo $content->about_first_block_text; ?>

				<div class="about-item__bottom">
					<span><?php echo e($content->about_first_link_info); ?></span>
					<a href="<?php echo e($content->about_first_link); ?>"><img src="<?php echo e(asset('/storage/img/arrow-right-long.svg')); ?>" alt=""></a>
				</div>
			</div>
		</div>
		<div class="about-item">
			<div class="about-item__img">
				<img src="<?php echo e(asset('/storage/img/about-img-2.jpg')); ?>" alt="">
			</div>
			<div class="about-item__content">
				<div class="about-item__logo">
					<img src="<?php echo e(asset('/storage/img/about-logo-2.svg')); ?>" alt="">
				</div>
                <?php echo $content->about_second_block_text; ?>

				<div class="about-item__bottom">
					<span><?php echo e($content->about_second_link_info); ?></span>
					<a href="<?php echo e($content->about_second_link); ?>"><img src="<?php echo e(asset('/storage/img/arrow-right-long.svg')); ?>" alt=""></a>
				</div>
			</div>
		</div>
	</div>
</div>


<section class="s-download">
	<div class="download-card">
		<div class="download-card__icon">
			<img src="<?php echo e(asset('/storage/img/download-icon.svg')); ?>" alt="">
		</div>
		<div class="download-card__text">
			<p><?php echo e(__('content.download_can')); ?></p>
			<h2><?php echo e(__('content.document_title')); ?></h2>
			<a download="13387 EBRD (GPA Facility Factsheet ARTWORK)" href="<?php echo e(asset('storage/docs/13387 EBRD (GPA Facility Factsheet ARTWORK).pdf')); ?>" class="btn btn-transparent"><?php echo e(__('content.download_button')); ?></a>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/alvariumsoft.dev/data/www/bb2019.alvariumsoft.dev/resources/views/pages/home-page.blade.php ENDPATH**/ ?>