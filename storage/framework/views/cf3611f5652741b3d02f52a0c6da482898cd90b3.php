<?php $__env->startSection('content'); ?>
    <section class="s-top" style="background-image: url(<?php echo e(asset($content->image)); ?>);">
        <div class="container">
            <h1 class="main-title"><?php echo e(__('content.gpa_title1')); ?> <br> <?php echo e(__('content.gpa_title2')); ?></h1>
        </div>
        <a href="#s-accordion" class="s-top-arrow"><img src="<?php echo e(asset('/storage/img/down-arrow-icon.svg')); ?>" alt=""></a>
    </section>

    <section class="s-accordion" id="s-accordion">
        <div class="container">
            <div class="blue-text">
                <p>
                    <?php echo e($content->title); ?>

                </p>
            </div>
            <div class="accordion-wrap">
                <?php $__currentLoopData = $blocks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $block): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="accordion-item">
                    <div class="accordion-title">
                        <h4 class="accordion-title__text"><?php echo e($block->title); ?></h4>
                        <div class="accordion-arrow"><img src="<?php echo e(asset('/storage/img/down-arrow-long.svg')); ?>" alt=""></div>
                    </div>
                    <div class="accordion-content">
                        <div class="accordion-content__inner">
                            <div class="accordion-content__left">
                                <?php echo $block->first_block; ?>

                            </div>
                            <div class="accordion-content__right">
                                <?php echo $block->second_block; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/alvariumsoft.dev/data/www/bb2019.alvariumsoft.dev/resources/views/pages/gpa-agreement.blade.php ENDPATH**/ ?>