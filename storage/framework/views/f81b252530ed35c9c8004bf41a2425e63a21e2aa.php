<?php $languages = \App\Facades\LangActionsService::getListLanguages(); ?>
<?php echo $__env->make('crud::fields.inc.wrapper_start', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <label><?php echo e($field['label']); ?></label>
    <select class="form-control" id="select_language">
    	<?php if(count($languages)): ?>
    		<?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    			<option value="<?php echo e($lang); ?>"
					<?php if(isset($field['value']) && $lang==$field['value']): ?>
						 selected
					<?php endif; ?>
    			><?php echo e($lang); ?></option>
    		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    	<?php endif; ?>
	</select>
<?php echo $__env->make('crud::fields.inc.wrapper_end', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


    <?php $__env->startPush('crud_fields_scripts'); ?>
        <script>
            function redirect_to_new_page_with_language_parameter() {
                var new_language = $("#select_language").val();
                var current_url = "<?php echo e(Request::url()); ?>";
                window.location.href = strip_last_language_parameter(current_url)+'?language='+new_language;
            }
            function strip_last_language_parameter(url) {
                if (url.indexOf("/create/") > -1 || url.indexOf("/edit/") > -1) {
                    var url_array = url.split('/');
                    url_array = url_array.slice(0, -1);
                    var clean_url = url_array.join('/');
                    return clean_url;
                }
                return url;
            }
            jQuery(document).ready(function($) {
                $("#select_language").change(function(e) {
                    var select_language_confirmation = confirm("Change language?");
                    if (select_language_confirmation == true) {
                        redirect_to_new_page_with_language_parameter();
                    } else {
                        // txt = "You pressed Cancel!";
                    }
                });

            });
        </script>
    <?php $__env->stopPush(); ?>

<?php /**PATH /var/www/alvariumsoft.dev/data/www/bb2019.alvariumsoft.dev/resources/views/vendor/backpack/crud/fields/select_language.blade.php ENDPATH**/ ?>