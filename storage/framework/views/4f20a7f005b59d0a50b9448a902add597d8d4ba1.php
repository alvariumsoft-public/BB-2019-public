<footer class="footer">
    <div class="footer__top">
        <div class="container">
            <div class="footer-logo">
                <div class="footer-logo__item">
                    <img src="<?php echo e(asset('/storage/img/footer-logo-1.svg')); ?>" alt="">
                </div>
                <div class="footer-logo__item">
                    <img src="<?php echo e(asset('/storage/img/footer-logo-2.svg')); ?>" alt="">
                </div>
            </div>
            <nav class="footer-nav">
                <ul>
                    <li><a href="#"><?php echo e(__('content.contact')); ?></a></li>
                    <li><a href="#"><?php echo e(__('content.terms')); ?></a></li>
                    <li><a href="#"><?php echo e(__('content.privacy')); ?></a></li>
                </ul>
            </nav>
            <div class="copyright"><?php echo e(__('content.copyright')); ?></div>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="container">
            <div class="footer-info"><?php echo e(__('content.site_by')); ?> <?php echo e(__('content.disigned')); ?></div>
        </div>
    </div>
</footer>
<?php /**PATH /var/www/alvariumsoft.dev/data/www/bb2019.alvariumsoft.dev/resources/views/layouts/footer.blade.php ENDPATH**/ ?>