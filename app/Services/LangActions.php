<?php
namespace App\Services;

class LangActions {

    public function getListLanguages() {
        return ['en', 'fr', 'ru', 'es'];
    }

    public function getSurveyLanguageKeyForSave($lang, $item) {
        if($lang == 'en') {
            $lang_key = 'default';
        } else {
            if(isset($item[$lang])) {
                $lang_key = $lang;
            } else {
                $lang_key = 'default';
            }
        }
        return $lang_key;
    }


}
