<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomePageTranslation extends Model
{
    public $timestamps = false;
    public $fillable = [
        'title',
        'first_block',
        'second_block',
        'about_first_block_text',
        'about_second_block_text',
        'about_first_link_info',
        'about_second_link_info'
    ];
}
