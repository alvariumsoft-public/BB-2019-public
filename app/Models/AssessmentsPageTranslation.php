<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentsPageTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'first_block',
        'second_block'
    ];
}
