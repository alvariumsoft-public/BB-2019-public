<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'short_description',
        'first_block_title',
        'first_block_first_column',
        'first_block_second_column',
        'central_block_title',
        'central_block_first_title',
        'central_block_first_text',
        'central_block_second_title',
        'central_block_second_text',
        'central_block_third_title',
        'central_block_third_text',
        'last_block_title',
        'last_block_first_column',
        'last_block_second_column'
    ];
}
