<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TechOperationTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'description',
        'first_block',
        'second_block'
    ];
}
