<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GpaAgreementPageTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title'
    ];
}
