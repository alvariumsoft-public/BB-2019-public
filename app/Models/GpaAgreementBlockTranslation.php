<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GpaAgreementBlockTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'first_block',
        'second_block'
    ];
}
