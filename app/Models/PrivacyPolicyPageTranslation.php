<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivacyPolicyPageTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'first_block',
        'second_block'
    ];
}
