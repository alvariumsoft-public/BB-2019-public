<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentResourcesTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title'
    ];
}
