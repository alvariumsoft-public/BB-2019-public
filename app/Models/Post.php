<?php

namespace App\Models;

use App\Traits\ImageMutator;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Post extends Model  implements TranslatableContract
{
    use CrudTrait;
    use Translatable;
    use Sluggable;
    use ImageMutator;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'posts';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $published = 1;
//    protected $fillable = [
//        'status'
//    ];
    protected $guarded = ['id'];
    // protected $hidden = [];
    // protected $dates = [];

    public $translatedAttributes = [
        'title',
        'short_description',
        'first_block_title',
        'first_block_first_column',
        'first_block_second_column',
        'central_block_title',
        'central_block_first_title',
        'central_block_first_text',
        'central_block_second_title',
        'central_block_second_text',
        'central_block_third_title',
        'central_block_third_title',
        'central_block_third_text',
        'last_block_first_column',
        'last_block_second_column'
        ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', $this->published);
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
