<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UploadPublish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currentUser = get_current_user();
        $files = Storage::disk('resources')->allFiles();
        foreach ($files as $file) {
            $contents = Storage::disk('resources')->get($file);
            Storage::disk('local')->makeDirectory(File::dirname('public/' . $file));
            Storage::disk('local')->put('public/' . $file, $contents, 0);
            if ( function_exists('posix_getpwuid') && ($currentUser == posix_getpwuid(fileowner(base_path('storage/app/public/' . $file)))['name']) ) {
                File::chmod(base_path('storage/app/public/' . $file), 0775);
            }
        }
        $directories = Storage::disk('local')->allDirectories();
        foreach ($directories as $directory) {
            if ( function_exists('posix_getpwuid') && ($currentUser == posix_getpwuid(fileowner(base_path('storage/app/' . $directory)))['name']) ) {
                File::chmod(base_path('storage/app/' . $directory), 0775);
            }
        }
        echo 'Complete!' . PHP_EOL;
    }
}
