<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LangActionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('langActions', 'App\Services\LangActions');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
