<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:255',
            'image' => 'required',
            'short_description' => 'required|min:10|max:500',
            'first_block_title' => 'required|min:3|max:255',
            'first_block_first_column' => 'required|min:10|max:65535',
            'first_block_second_column' => 'required|min:10|max:65535',
            'central_block_title' => 'required|min:3|max:255',
            'central_block_first_title' => 'required|min:3|max:255',
            'central_block_first_image' => 'required',
            'central_block_first_text' => 'required|min:10|max:3000',
            'central_block_second_title' => 'min:3|max:255',
            'central_block_second_image' => 'required_with:central_block_second_text',
            'central_block_second_text' => 'min:10|max:700',
            'central_block_third_title' => 'min:3|max:255',
            'central_block_third_image' => 'required_with:central_block_third_text',
            'central_block_third_text' => 'min:10|max:700',
            'last_block_first_column' => 'required|min:10|max:65535',
            'last_block_second_column' => 'required|min:10|max:65535'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
