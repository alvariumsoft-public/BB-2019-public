<?php

namespace App\Http\Controllers;

use Session;

class LanguageController extends Controller
{
    public function setLocale($locale) {
        Session::put('locale',$locale);
        return redirect()->back();
    }
}
