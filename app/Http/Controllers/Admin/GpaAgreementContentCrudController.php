<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GpaAgreementContentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\App;

/**
 * Class GpaAgreementContentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class GpaAgreementContentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\GpaAgreementBlock::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/gpaagreementcontent');
        CRUD::setEntityNameStrings('gpaagreementcontent', 'gpa agreement content');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns([
                                    [
                                        'name' => 'title',
                                        'label' => 'Title',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'first_block',
                                        'label' => 'First block',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'second_block',
                                        'label' => 'Second block',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'status',
                                        'type' => "boolean",
                                        'default' => true,
                                        'label' => "Published",
                                    ],
                                ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(GpaAgreementContentRequest::class);

        $language = \Request::input('language') ?? 'en';
        App::setLocale($language);

        $this->crud->addField([
                                  'name' => 'language',
                                  'type' => 'hidden',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addField([
                                  'name' => 'lang',
                                  'label' => 'Language',
                                  'type' => 'select_language',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addFields([
                                   [
                                       'name' => 'title',
                                       'label' => 'Title',
                                       'type' => 'text'
                                   ],
                                   [
                                       'name' => 'first_block',
                                       'label' => 'First block',
                                       'type' => 'wysiwyg'
                                   ],
                                   [
                                       'name' => 'second_block',
                                       'label' => 'Second block',
                                       'type' => 'wysiwyg'
                                   ],
                                   [
                                       'name' => 'status',
                                       'label' => "Status",
                                       'type' => 'select_from_array',
                                       'options' => [1 => 'Published', 0 => 'Unpublished'],
                                   ],
                               ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }
}
