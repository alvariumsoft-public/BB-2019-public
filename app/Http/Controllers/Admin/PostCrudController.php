<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\App;

/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Post::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/post');
        CRUD::setEntityNameStrings('post', 'posts');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns([
                                    [
                                        'name' => 'title',
                                        'label' => 'Title',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'short_description',
                                        'label' => 'Short description',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'status',
                                        'label' => "Status",
                                        'type' => 'select_from_array',
                                        'options' => [1 => 'Published', 0 => 'Unpublished'],
                                    ],
                                    [
                                        'name' => 'created_at',
                                        'label' => 'Created',
                                        'type' => 'text'
                                    ],
                                ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PostRequest::class);

        $language = \Request::input('language') ?? 'en';
        App::setLocale($language);

        $this->crud->addField([
                                  'name' => 'language',
                                  'type' => 'hidden',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addField([
                                  'name' => 'lang',
                                  'label' => 'Language',
                                  'type' => 'select_language',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addFields([
                                    [
                                        'name' => 'title',
                                        'label' => 'Title',
                                        'type' => 'text'
                                    ],
                                   [
                                       'name' => 'image',
                                       'label' => 'Main Image',
                                       'type' => 'image',
                                       'upload' => true,
                                       'crop' => true,
                                       'aspect_ratio' => 1.5
                                   ],
                                    [
                                        'name' => 'short_description',
                                        'label' => 'Short description',
                                        'type' => 'textarea'
                                    ],
                                   [
                                       'name' => 'status',
                                       'label' => "Status",
                                       'type' => 'select_from_array',
                                       'options' => [1 => 'Published', 0 => 'Unpublished'],
                                   ],
                                   [
                                       'name' => 'first_block_title',
                                       'label' => 'Title',
                                       'type' => 'text',
                                       'tab' => 'First block'
                                   ],
                                   [
                                       'name' => 'first_block_first_column',
                                       'label' => 'First Column',
                                       'type' => 'wysiwyg',
                                       'tab' => 'First block'
                                   ],
                                   [
                                       'name' => 'first_block_second_column',
                                       'label' => 'Second Column',
                                       'type' => 'wysiwyg',
                                       'tab' => 'First block'
                                   ],
                                   [
                                       'name' => 'central_block_title',
                                       'label' => 'Title',
                                       'type' => 'text',
                                       'tab' => 'Central block'
                                   ],
                                   [
                                       'name' => 'central_block_first_title',
                                       'label' => 'First Item Title',
                                       'type' => 'text',
                                       'tab' => 'Central block'
                                   ],
                                   [
                                       'name' => 'central_block_first_image',
                                       'label' => 'First Item Image',
                                       'type' => 'image',
                                       'tab' => 'Central block',
                                       'upload' => true,
                                       'crop' => true,
                                       'aspect_ratio' => 0.8
                                   ],
                                   [
                                       'name' => 'central_block_first_text',
                                       'label' => 'First Item Text',
                                       'type' => 'wysiwyg',
                                       'tab' => 'Central block'
                                   ],
                                   [
                                       'name' => 'central_block_second_title',
                                       'label' => 'Second Item Title',
                                       'type' => 'text',
                                       'tab' => 'Central block'
                                   ],
                                   [
                                       'name' => 'central_block_second_image',
                                       'label' => 'Second Item Image',
                                       'type' => 'image',
                                       'tab' => 'Central block',
                                       'upload' => true,
                                       'crop' => true,
                                       'aspect_ratio' => 0.8
                                   ],
                                   [
                                       'name' => 'central_block_second_text',
                                       'label' => 'Second Item Text',
                                       'type' => 'wysiwyg',
                                       'tab' => 'Central block'
                                   ],
                                   [
                                       'name' => 'central_block_third_title',
                                       'label' => 'Third Item Title',
                                       'type' => 'text',
                                       'tab' => 'Central block'
                                   ],
                                   [
                                       'name' => 'central_block_third_image',
                                       'label' => 'Third Item Image',
                                       'type' => 'image',
                                       'tab' => 'Central block',
                                       'upload' => true,
                                       'crop' => true,
                                       'aspect_ratio' => 0.8
                                   ],
                                   [
                                       'name' => 'central_block_third_text',
                                       'label' => 'Third Item Text',
                                       'type' => 'wysiwyg',
                                       'tab' => 'Central block'
                                   ],
                                   [
                                       'name' => 'last_block_first_column',
                                       'label' => 'First Column',
                                       'type' => 'wysiwyg',
                                       'tab' => 'Last block'
                                   ],
                                   [
                                       'name' => 'last_block_second_column',
                                       'label' => 'Second Column',
                                       'type' => 'wysiwyg',
                                       'tab' => 'Last block'
                                   ],
                               ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function store()
    {
        $this->crud->hasAccessOrFail('create');
        $data = $this->crud->getStrippedSaveRequest();
        $data['name'] = $data['title'];
        $item = $this->crud->create($data);
        $item->save();
        $this->data['entry'] = $this->crud->entry = $item;
        \Alert::success(trans('backpack::crud.update_success'))->flash();
        $this->crud->setSaveAction();
        return $this->crud->performSaveAction($item->getKey());
    }
}
