<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PrivacyPolicyPageRequest;
use App\Http\Requests\TermsPageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\App;

/**
 * Class TermsPageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TermsPageCrudController extends CrudController
{

    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\TermsPage::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/termspage');
        CRUD::setEntityNameStrings('termspage', 'terms and conditions page');
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('create');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TermsPageRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(TermsPageRequest::class);
        $language = \Request::input('language') ?? 'en';
        App::setLocale($language);

        $this->crud->addField([
                                  'name' => 'language',
                                  'type' => 'hidden',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addField([
                                  'name' => 'lang',
                                  'label' => 'Language',
                                  'type' => 'select_language',
                                  'value' => App::getLocale(),
                              ]);
        $this->crud->addFields([
                                    [
                                        'name' => 'first_block',
                                        'label' => 'First Column',
                                        'type' => 'wysiwyg',
                                    ],
                                   [
                                       'name' => 'second_block',
                                       'label' => 'Second Column',
                                       'type' => 'wysiwyg',
                                   ]
                              ]);
    }
}
