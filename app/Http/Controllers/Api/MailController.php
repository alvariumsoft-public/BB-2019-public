<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Notifications\SendContact;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{
    public function sendMail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required|min:3',
            'enquiry' =>  'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                                        'status' => 'error',
                                        'message' => $validator->errors()
                                    ], Response::HTTP_ACCEPTED);
        }
        $when = now()->addMinutes(1);

        Notification::route('mail', env('MAIL_ADMIN'))
            ->notify((new SendContact($request->all()))->delay($when));
        return [
            'status' => 'success',
            'message' => __('content.mail_sent'),
        ];
    }
}
