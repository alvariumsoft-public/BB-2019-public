<?php

use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $first_block_title = '<p>GPA in Ukraine: Facilitating GPA Implementation and Capacity Building of Domestic Suppliers.</p>';
//        $first_block_first_column = '<p>
//                        Ukraine became a Party to the GPA in May 2016. The Government expected that in the short term, the
//                        Agreement, as an effective tool, should have brought benefits to the national economy of Ukraine as
//                        well as a positive impact on foreign trade and exports. Following observations made on the impact of
//                        GPA membership on Ukraine’s economy, the following issues were identified:
//                    </p>
//                    <ul>
//                        <li>
//                            Economic operators do not have reliable information about the Agreement and the
//                            available business opportunities;
//                        </li>
//                        <li>
//                            Lack of knowledge about the rules and procedures for participating in procurement of
//                            other GPA Parties;
//                        </li>
//                        <li>
//                            Lack of a competence center in Ukraine, where, if necessary, potential bidders can get
//                            advice and support on participating procurement processes of GPA Parties;
//                        </li>
//                    </ul>';
//
//        $first_block_second_column = '<ul>
//                        <li>
//                            Insufficient communication between the Government, businesses associations and
//                            economic operators regarding participation in international public procurement
//                            of GPA Parties.
//                        </li>
//                    </ul>
//                    <p>
//                        The Ministry of Economic Development and Trade of Ukraine applied to the EBRD for technical support
//                        on ‘facilitating the GPA implementation and capacity building of suppliers in Ukraine’.
//                    </p>
//                    <p>
//                        The objectives of the TC Project assignment are:
//                    </p>
//                    <ol>
//                        <li>
//                            The promotion and facilitation of the implementation of the GPA in Ukraine for the
//                            development of international public procurement market in Ukraine.
//                        </li>
//                        <li>
//                            Support, advice and capacity building of the MEDTA on the GPA implementation.
//                        </li>
//                    </ol>';
//        $central_block_title = 'Among some project assignments, consultants achieved the following results/outcomes:';
//
//        $central_block_first_title = '1.Provided methodological, training and informational support:';
//        $central_block_first_text = '<ul>
//                            <li>
//                                Prepared six guidelines for eight economies;
//                            </li>
//                            <li>
//                                Conducted more than 20 workshops and trainings on GPA promotion and training of
//                                economic operators;
//                            </li>
//                            <li>
//                                Constantly sharing information through various communication channels about news
//                                and opportunities for Ukrainian economic operators;
//                            </li>
//                            <li>
//                                Participating in meetings and round tables with representatives of the Government,
//                                Parliament and businesses associations on the implementation of GPA in Ukraine;
//                            </li>
//                            <li>
//                                Offering practical and updated information through social media: available
//                                at <a href="">www.facebook.com/gpainua/.</a>
//                            </li>
//                        </ul>';
//
//        $central_block_first_image = '/storage/img/blog-img-1.jpg';
//
//        $central_block_second_image = '/storage/img/blog-img-2.jpg';
//
//        $central_block_third_image = '/storage/img/blog-img-3.jpg';
//
//        $central_block_second_title = '2. Practical support in tender processes:';
//
//        $central_block_second_text = ' <p>
//                            Provided advisory support to 42 companies in more than 220 international tenders from other GPA
//                            Parties.
//                        </p>';
//
//        $central_block_third_title = '3. Analytical research and reports prepared and delivered:';
//
//        $central_block_third_text = '<ul>
//                            <li>
//                                Researched practical ways to support Ukrainian suppliers experiencing difficulties in
//                                foreign tender processes with solutions that could overcome these obstacles.
//                            </li>
//                            <li>
//                                The results obtained and generalized conclusions shall be a significant incentive for
//                                further implementation of the GPA as they can be applied by any Ukrainian company seeking to
//                                participate in foreign GPA-covered procurement.
//                            </li>
//                        </ul>';
//
//        $last_block_first_column = '<p>
//                        The results of successful participation of Ukrainian suppliers in GPA covered tenders confirmed the
//                        initial hypothesis of the research conducted by EBRD experts from the GPAinUA Project:
//                    </p>
//                    <ul>
//                        <li>
//                            “Not one international trade agreement has visible economic effects if national
//                            companies do not implement it”; and,
//                        </li>
//                        <li>
//                            “Only practical experience shows the ability and capacity of businesses and the ways to
//                            i. mprove it”.
//                        </li>
//                        <li>
//                            Lack of a competence center in Ukraine, where, if necessary, potential bidders can get
//                            advice and support on participating procurement processes of GPA Parties;
//                        </li>
//                    </ul>
//                    <p>
//                        The positive results might become a positive factor and might encourage other national business to
//                        participate in GPA tenders.
//                    </p>';
//
//        $last_block_second_column = '<p>
//                        EBRD support under the EBRD GPA TC Facility plays an important role in building practical capacities
//                        of
//                        Ukrainian SMEs and their awareness of the available tools provided by the GPA to access foreign
//                        markets.
//                    </p>
//                    <p>
//                        The experience gained by EBRD consultants in Ukraine may be extrapolated to other GPA acceding
//                        countries
//                        or new GPA Parties. The establishment of local supporting teams to promote participation of local
//                        SMEs
//                        in foreign tenders–similar to GPAinUA in Ukraine–will allow achieving practical and economic
//                        results,
//                        accelerating the effective implementation of the GPA.
//                    </p>';
//
//        for ($i = 1; $i <= 9; $i++){
//            $post = \App\Models\Post::create([
//                                                 'image' => '/storage/img/about-img-1.jpg',
//                                                 'central_block_first_image' => $central_block_first_image,
//                                                 'central_block_second_image' => $central_block_second_image,
//                                                 'central_block_third_image' => $central_block_third_image,
//                                                 'name' => 'Post '.$i
//                                             ]);
//
//            $post->translateOrNew('en')->title = 'Post '.$i;
//            $post->translateOrNew('en')->short_description = 'The Facility supported the preparation of the national GPA negotiation strategy and provided legal and policy advice throughout the negotiation process.';
//            $post->translateOrNew('en')->first_block_title = $first_block_title;
//            $post->translateOrNew('en')->first_block_first_column = $first_block_first_column;
//            $post->translateOrNew('en')->first_block_second_column = $first_block_second_column;
//            $post->translateOrNew('en')->central_block_title = $central_block_title;
//            $post->translateOrNew('en')->central_block_first_title = $central_block_first_title;
//            $post->translateOrNew('en')->central_block_first_text = $central_block_first_text;
//            $post->translateOrNew('en')->central_block_second_title = $central_block_second_title;
//            $post->translateOrNew('en')->central_block_second_text = $central_block_second_text;
//            $post->translateOrNew('en')->central_block_third_title = $central_block_third_title;
//            $post->translateOrNew('en')->central_block_third_text = $central_block_third_text;
//            $post->translateOrNew('en')->last_block_first_column = $last_block_first_column;
//            $post->translateOrNew('en')->last_block_second_column = $last_block_second_column;
//            $post->created_at = '2020-10-06 13:29:5'.$i;
//            $post->save();
//        }
//
//        $post = \App\Models\Post::create([
//                                             'image' => '/storage/img/about-img-1.jpg',
//                                             'central_block_first_image' => $central_block_first_image,
//                                             'central_block_second_image' => $central_block_second_image,
//                                             'central_block_third_image' => $central_block_third_image,
//                                             'name' => 'North Macedonia’s accession negotiations'
//                                         ]);
//
//        $post->translateOrNew('en')->title = 'North Macedonia’s accession negotiations';
//        $post->translateOrNew('en')->short_description = 'Since June 2018, North Macedonia has received support in its bilateral negotiations with GPA Parties, including on presenting aceptable coverage offers. Related capacity building activities have also been provided.';
//        $post->translateOrNew('en')->first_block_title = $first_block_title;
//        $post->translateOrNew('en')->first_block_first_column = $first_block_first_column;
//        $post->translateOrNew('en')->first_block_second_column = $first_block_second_column;
//        $post->translateOrNew('en')->central_block_title = $central_block_title;
//        $post->translateOrNew('en')->central_block_first_title = $central_block_first_title;
//        $post->translateOrNew('en')->central_block_first_text = $central_block_first_text;
//        $post->translateOrNew('en')->central_block_second_title = $central_block_second_title;
//        $post->translateOrNew('en')->central_block_second_text = $central_block_second_text;
//        $post->translateOrNew('en')->central_block_third_title = $central_block_third_title;
//        $post->translateOrNew('en')->central_block_third_text = $central_block_third_text;
//        $post->translateOrNew('en')->last_block_first_column = $last_block_first_column;
//        $post->translateOrNew('en')->last_block_second_column = $last_block_second_column;
//        $post->created_at = '2020-10-06 13:30:50';
//        $post->save();
//
//        $post = \App\Models\Post::create([
//                                             'image' => '/storage/img/news2.png',
//                                             'central_block_first_image' => $central_block_first_image,
//                                             'central_block_second_image' => $central_block_second_image,
//                                             'central_block_third_image' => $central_block_third_image,
//                                             'name' => 'Moldova Accession to the GPA'
//                                         ]);
//
//        $post->translateOrNew('en')->title = 'Moldova Accession to the GPA';
//        $post->translateOrNew('en')->short_description = 'The Facility supported the preparation of the national GPA negotiation strategy and provided legal and policy advice throughout the negotiation process.';
//        $post->translateOrNew('en')->first_block_title = $first_block_title;
//        $post->translateOrNew('en')->first_block_first_column = $first_block_first_column;
//        $post->translateOrNew('en')->first_block_second_column = $first_block_second_column;
//        $post->translateOrNew('en')->central_block_title = $central_block_title;
//        $post->translateOrNew('en')->central_block_first_title = $central_block_first_title;
//        $post->translateOrNew('en')->central_block_first_text = $central_block_first_text;
//        $post->translateOrNew('en')->central_block_second_title = $central_block_second_title;
//        $post->translateOrNew('en')->central_block_second_text = $central_block_second_text;
//        $post->translateOrNew('en')->central_block_third_title = $central_block_third_title;
//        $post->translateOrNew('en')->central_block_third_text = $central_block_third_text;
//        $post->translateOrNew('en')->last_block_first_column = $last_block_first_column;
//        $post->translateOrNew('en')->last_block_second_column = $last_block_second_column;
//        $post->created_at = '2020-10-06 13:30:51';
//        $post->save();
//
//        $post = \App\Models\Post::create([
//                                             'image' => '/storage/img/about-img-1.jpg',
//                                             'central_block_first_image' => $central_block_first_image,
//                                             'central_block_second_image' => $central_block_second_image,
//                                             'central_block_third_image' => $central_block_third_image,
//                                             'name' => 'GPA in UA'
//                                         ]);
//
//        $post->translateOrNew('en')->title = 'GPA in UA';
//        $post->translateOrNew('en')->short_description = 'The EBRD worked closely to facilitate GPA implementation and capacity building of domestic suppliers in Ukraine.';
//        $post->translateOrNew('en')->first_block_title = $first_block_title;
//        $post->translateOrNew('en')->first_block_first_column = $first_block_first_column;
//        $post->translateOrNew('en')->first_block_second_column = $first_block_second_column;
//        $post->translateOrNew('en')->central_block_title = $central_block_title;
//        $post->translateOrNew('en')->central_block_first_title = $central_block_first_title;
//        $post->translateOrNew('en')->central_block_first_text = $central_block_first_text;
//        $post->translateOrNew('en')->central_block_second_title = $central_block_second_title;
//        $post->translateOrNew('en')->central_block_second_text = $central_block_second_text;
//        $post->translateOrNew('en')->central_block_third_title = $central_block_third_title;
//        $post->translateOrNew('en')->central_block_third_text = $central_block_third_text;
//        $post->translateOrNew('en')->last_block_first_column = $last_block_first_column;
//        $post->translateOrNew('en')->last_block_second_column = $last_block_second_column;
//        $post->created_at = '2020-10-06 13:30:52';
//        $post->save();

        \Illuminate\Support\Facades\DB::insert(file_get_contents(base_path('database/dumps/posts.sql')));
        \Illuminate\Support\Facades\DB::insert(file_get_contents(base_path('database/dumps/posts-translations.sql')));
    }
}
