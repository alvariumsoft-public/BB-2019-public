<?php

use Illuminate\Database\Seeder;

class HomePageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $homePage = \App\Models\HomePage::create([
                                         'image' => '/storage/img/top-intro-bg.jpg',
                                         'about_first_link' => '',
                                         'about_second_link' => '',
                                     ]);

        $title = 'Procurement for governmental purposes constitutes a significant share of public expenditure, approximately 10-15 per cent of the GDP of an economy. As such, governments are expected to conduct procurement following high standards that ensure open competition, transparency and the integrity of the process. Government procurement can also serve as an instrument for internal policy reform with a view to improving good governance in the procurement system.';
        $first_block = '<p>
					The Legal Transition Programme (LTP) of the European Bank for Reconstruction and Development (EBRD) focuses on the
					development of legal rules and the establishment of legal institutions that create a transparent and predictable
					investment climate in the economies the Bank operates. A key focus of the LTP activities is helping governments
					improve the way they manage public contracting, making procurement systems more transparent, efficient and
					competitive so that taxpayers get better value for money. An important step in achieving these goals is accession
					to the World Trade Organization Agreement on Government Procurement (WTO GPA). The fundamental aim of the GPA is to
					mutually open government procurement markets among its Parties, while ensuring open, fair and transparent
					conditions of competition.
				</p>
				<p>
					Since 2011, EBRD’s LTP has been actively supporting countries pursuing accession to the GPA by working in
					cooperation with the WTO Secretariat. The re-negotiation and subsequent adoption of the revised text of the GPA in
					2012 motivated a gradual broadening of its membership as a number of countries considered accession to the
					Agreement as a step in their internal reform process.
				</p>';
        $second_block = '<p>
					As such, the growing interest in GPA accession in the EBRD’s countries of operation demanded a more structured and
					ongoing cooperation between the EBRD and the WTO Secretariat. As a result, the EBRD GPA Technical Cooperation
					Facility (EBRD GPA TC Facility) was launched in May 2014.
				</p>
				<p>
					The EBRD GPA TC Facility provides significant support to countries seeking accession to the agreement by providing
					(1) capacity building activities, (2) country-specific technical cooperation projects and (3) accession negotiation
					assistance to countries wishing to accede to the Agreement. Moreover, the Facility supports governments in
					overcoming any institutional, legal and/or trade challenges related to GPA accession they
					may face.
				</p>
				<p>
					Besides providing ongoing capacity building and technical assistance in matters related to the GPA, the Facility
					contributes as well with the coordination, planning and delivery of key outreach activities, focused on the needs
					of interested Parties, ensuring effective institutional and professional relationships as well as facilitating
					liaison and coordination with other relevant international and regional organizations, NGOs and academia in the
					area of public procurement.
				</p>';

        $about_first_block_text = '<p>
					The European Bank for Reconstruction and Development is an international financial institution that
					promotes
					transition to a well-functioning market economy. The Bank, active in 38 economies across three
					continents –
					from
					the Southern and Eastern Mediterranean, to Central and Eastern Europe, to Central Asia – helps
					countries
					develop
					commercial laws and institutions that create a sound investment climate and promote sustainable
					economic
					growth.
				</p>';
        $about_second_block_text = '<p>
					The World Trade Organization is an intergovernmental organization whose primary purpose is to open
					trade, ensuring
					that it flows as smoothly, predictably and freely as possible. It acts as a forum for its Member
					governments to
					negotiate trade agreements while operating a system of trade rules. At its core are the WTO agreements,
					negotiated
					and signed by the bulk of the world’s trading nations. These agreements provide the legal ground rules
					for
					international commerce.
				</p>';
        $about_first_link_info = 'More information can be found at: www.ebrd.com';
        $about_second_link_info = 'More information can be found at: www.wto.org';

        $homePage->translateOrNew('en')->title = $title;
        $homePage->translateOrNew('en')->first_block = $first_block;
        $homePage->translateOrNew('en')->second_block = $second_block;
        $homePage->translateOrNew('en')->about_first_block_text = $about_first_block_text;
        $homePage->translateOrNew('en')->about_second_block_text = $about_second_block_text;
        $homePage->translateOrNew('en')->about_first_link_info = $about_first_link_info;
        $homePage->translateOrNew('en')->about_second_link_info = $about_second_link_info;
        $homePage->save();
    }
}
