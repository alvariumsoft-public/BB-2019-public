<?php

use Illuminate\Database\Seeder;

class TermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $page = \App\Models\TermsPage::create([
                                                      'image' => '/storage/img/s-top-bg-5.jpg',
                                                      'slug' => 'terms-and-conditions'
                                                  ]);
        $first_block = '<p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris accumsan, quam a finibus tincidunt,
                        urna nibh rutrum nisl, sit amet tincidunt eros ipsum et risus. Mauris laoreet hendrerit suscipit.
                        Etiam vitae nunc quis erat pellentesque lacinia. Proin suscipit ex vel risus egestas elementum.
                        Vivamus non consequat purus, vitae pharetra sem. Vivamus et sagittis lacus. Morbi mollis orci quis
                        porta varius. Proin vitae tellus et felis feugiat dictum quis at sapien. Pellentesque vel nisl leo.
                        Donec viverra diam at urna faucibus luctus. Etiam ac mi tempus, hendrerit enim sit amet, eleifend
                        eros. Suspendisse vel mauris in erat facilisis consequat sed a ante. Quisque posuere nibh lacus, in
                        ultricies mi convallis non.
                    </p>
                    <p>
                        Donec congue porttitor sapien non tempus. Donec quis congue ipsum. Mauris vitae dui tortor. Vivamus
                        ut ligula a purus vestibulum venenatis. Maecenas dignissim non est id ullamcorper. Donec ut volutpat
                        dui. Morbi sed congue ligula, at pellentesque dui. Curabitur sed auctor magna. Aenean quis sem
                        efficitur, dapibus quam non, facilisis velit. Etiam feugiat tristique pretium.
                    </p>
                    <p>
                        Maecenas vel justo orci. Praesent sagittis efficitur erat quis vehicula. Etiam dignissim metus sit
                        amet purus tempus, nec egestas magna sollicitudin. Pellentesque vel eleifend diam. Duis auctor
                        tincidunt ornare. Quisque cursus vulputate quam in convallis. Donec ac maximus nibh. Fusce sed ante
                        et ligula euismod venenatis rutrum eu augue.
                    </p>
                    <p>
                        Aenean sit amet massa ac metus luctus laoreet at vel arcu. Donec vitae luctus magna, non elementum
                        nulla. Nullam fringilla elementum turpis. Cras varius, mauris ut consequat luctus, ex erat eleifend
                        turpis, id sodales quam sem in massa. Vestibulum rhoncus tellus massa, non pellentesque nisl
                        ullamcorper vel. Quisque fermentum, diam at scelerisque porta, velit sapien pretium metus, et
                        aliquam eros nisl sed libero. Suspendisse in risus quis sapien tempus semper ut sed metus. Curabitur
                        tincidunt ipsum non facilisis ultricies. Suspendisse sit amet lorem facilisis, laoreet eros
                        tincidunt, placerat felis. Curabitur nec nibh ac diam hendrerit porttitor. Duis vehicula metus
                        lorem, vel suscipit quam aliquet quis.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris accumsan, quam a finibus tincidunt,
                        urna nibh rutrum nisl, sit amet tincidunt eros ipsum et risus. Mauris laoreet hendrerit suscipit.
                        Etiam vitae nunc quis erat pellentesque lacinia. Proin suscipit ex vel risus egestas elementum.
                        Vivamus non consequat purus, vitae pharetra sem. Vivamus et sagittis lacus. Morbi mollis orci quis
                        porta varius. Proin vitae tellus et felis feugiat dictum quis at sapien. Pellentesque vel nisl leo.
                        Donec viverra diam at urna faucibus luctus. Etiam ac mi tempus, hendrerit enim sit amet, eleifend
                        eros. Suspendisse vel mauris in erat facilisis consequat sed a ante. Quisque posuere nibh lacus, in
                        ultricies mi convallis non.
                    </p>
                    <p>
                        Donec congue porttitor sapien non tempus. Donec quis congue ipsum. Mauris vitae dui tortor. Vivamus
                        ut ligula a purus vestibulum venenatis. Maecenas dignissim non est id ullamcorper. Donec ut volutpat
                        dui. Morbi sed congue ligula, at pellentesque dui. Curabitur sed auctor magna. Aenean quis sem
                        efficitur, dapibus quam non, facilisis velit. Etiam feugiat tristique pretium.
                    </p>
                    <p>
                        Maecenas vel justo orci. Praesent sagittis efficitur erat quis vehicula. Etiam dignissim metus sit
                        amet purus tempus, nec egestas magna sollicitudin. Pellentesque vel eleifend diam. Duis auctor
                        tincidunt ornare. Quisque cursus vulputate quam in convallis. Donec ac maximus nibh. Fusce sed ante
                        et ligula euismod venenatis rutrum eu augue.
                    </p>
                    <p>
                        Aenean sit amet massa ac metus luctus laoreet at vel arcu. Donec vitae luctus magna, non elementum
                        nulla. Nullam fringilla elementum turpis. Cras varius, mauris ut consequat luctus, ex erat eleifend
                        turpis, id sodales quam sem in massa. Vestibulum rhoncus tellus massa, non pellentesque nisl
                        ullamcorper vel. Quisque fermentum, diam at scelerisque porta, velit sapien pretium metus, et
                        aliquam eros nisl sed libero. Suspendisse in risus quis sapien tempus semper ut sed metus. Curabitur
                        tincidunt ipsum non facilisis ultricies. Suspendisse sit amet lorem facilisis, laoreet eros
                        tincidunt, placerat felis. Curabitur nec nibh ac diam hendrerit porttitor. Duis vehicula metus
                        lorem, vel suscipit quam aliquet quis.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris accumsan, quam a finibus tincidunt,
                        urna nibh rutrum nisl, sit amet tincidunt eros ipsum et risus. Mauris laoreet hendrerit suscipit.
                        Etiam vitae nunc quis erat pellentesque lacinia. Proin suscipit ex vel risus egestas elementum.
                        Vivamus non consequat purus, vitae pharetra sem. Vivamus et sagittis lacus. Morbi mollis orci quis
                        porta varius. Proin vitae tellus et felis feugiat dictum quis at sapien. Pellentesque vel nisl leo.
                        Donec viverra diam at urna faucibus luctus. Etiam ac mi tempus, hendrerit enim sit amet, eleifend
                        eros. Suspendisse vel mauris in erat facilisis consequat sed a ante. Quisque posuere nibh lacus, in
                        ultricies mi convallis non.
                    </p>
                    <p>
                        Donec congue porttitor sapien non tempus. Donec quis congue ipsum. Mauris vitae dui tortor. Vivamus
                        ut ligula a purus vestibulum venenatis. Maecenas dignissim non est id ullamcorper. Donec ut volutpat
                        dui. Morbi sed congue ligula, at pellentesque dui. Curabitur sed auctor magna. Aenean quis sem
                        efficitur, dapibus quam non, facilisis velit. Etiam feugiat tristique pretium.
                    </p>
                    <p>
                        Maecenas vel justo orci. Praesent sagittis efficitur erat quis vehicula. Etiam dignissim metus sit
                        amet purus tempus, nec egestas magna sollicitudin. Pellentesque vel eleifend diam. Duis auctor
                        tincidunt ornare. Quisque cursus vulputate quam in convallis. Donec ac maximus nibh. Fusce sed ante
                        et ligula euismod venenatis rutrum eu augue.
                    </p>
                    <p>
                        Aenean sit amet massa ac metus luctus laoreet at vel arcu. Donec vitae luctus magna, non elementum
                        nulla. Nullam fringilla elementum turpis. Cras varius, mauris ut consequat luctus, ex erat eleifend
                        turpis, id sodales quam sem in massa. Vestibulum rhoncus tellus massa, non pellentesque nisl
                        ullamcorper vel. Quisque fermentum, diam at scelerisque porta, velit sapien pretium metus, et
                        aliquam eros nisl sed libero. Suspendisse in risus quis sapien tempus semper ut sed metus. Curabitur
                        tincidunt ipsum non facilisis ultricies. Suspendisse sit amet lorem facilisis, laoreet eros
                        tincidunt, placerat felis. Curabitur nec nibh ac diam hendrerit porttitor. Duis vehicula metus
                        lorem, vel suscipit quam aliquet quis.
                    </p>';
        $second_block = '<p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris accumsan, quam a finibus tincidunt,
                        urna nibh rutrum nisl, sit amet tincidunt eros ipsum et risus. Mauris laoreet hendrerit suscipit.
                        Etiam vitae nunc quis erat pellentesque lacinia. Proin suscipit ex vel risus egestas elementum.
                        Vivamus non consequat purus, vitae pharetra sem. Vivamus et sagittis lacus. Morbi mollis orci quis
                        porta varius. Proin vitae tellus et felis feugiat dictum quis at sapien. Pellentesque vel nisl leo.
                        Donec viverra diam at urna faucibus luctus. Etiam ac mi tempus, hendrerit enim sit amet, eleifend
                        eros. Suspendisse vel mauris in erat facilisis consequat sed a ante. Quisque posuere nibh lacus, in
                        ultricies mi convallis non.
                    <p>
                        Donec congue porttitor sapien non tempus. Donec quis congue ipsum. Mauris vitae dui tortor. Vivamus
                        ut ligula a purus vestibulum venenatis. Maecenas dignissim non est id ullamcorper. Donec ut volutpat
                        dui. Morbi sed congue ligula, at pellentesque dui. Curabitur sed auctor magna. Aenean quis sem
                        efficitur, dapibus quam non, facilisis velit. Etiam feugiat tristique pretium.
                    </p>
                    <p>
                        Maecenas vel justo orci. Praesent sagittis efficitur erat quis vehicula. Etiam dignissim metus sit
                        amet purus tempus, nec egestas magna sollicitudin. Pellentesque vel eleifend diam. Duis auctor
                        tincidunt ornare. Quisque cursus vulputate quam in convallis. Donec ac maximus nibh. Fusce sed ante
                        et ligula euismod venenatis rutrum eu augue.
                    </p>
                    <p>
                        Aenean sit amet massa ac metus luctus laoreet at vel arcu. Donec vitae luctus magna, non elementum
                        nulla. Nullam fringilla elementum turpis. Cras varius, mauris ut consequat luctus, ex erat eleifend
                        turpis, id sodales quam sem in massa. Vestibulum rhoncus tellus massa, non pellentesque nisl
                        ullamcorper vel. Quisque fermentum, diam at scelerisque porta, velit sapien pretium metus, et
                        aliquam eros nisl sed libero. Suspendisse in risus quis sapien tempus semper ut sed metus. Curabitur
                        tincidunt ipsum non facilisis ultricies. Suspendisse sit amet lorem facilisis, laoreet eros
                        tincidunt, placerat felis. Curabitur nec nibh ac diam hendrerit porttitor. Duis vehicula metus
                        lorem, vel suscipit quam aliquet quis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Mauris accumsan, quam a finibus tincidunt, urna nibh rutrum nisl, sit amet tincidunt eros ipsum et
                        risus. Mauris laoreet hendrerit suscipit. Etiam vitae nunc quis erat pellentesque lacinia. Proin
                        suscipit ex vel risus egestas elementum. Vivamus non consequat purus, vitae pharetra sem. Vivamus et
                        sagittis lacus. Morbi mollis orci quis porta varius. Proin vitae tellus et felis feugiat dictum quis
                        at sapien. Pellentesque vel nisl leo. Donec viverra diam at urna faucibus luctus. Etiam ac mi
                        tempus, hendrerit enim sit amet, eleifend eros. Suspendisse vel mauris in erat facilisis consequat
                        sed a ante. Quisque posuere nibh lacus, in ultricies mi convallis non.
                    </p>
                    <p>
                        Donec congue porttitor sapien non tempus. Donec quis congue ipsum. Mauris vitae dui tortor. Vivamus
                        ut ligula a purus vestibulum venenatis. Maecenas dignissim non est id ullamcorper. Donec ut volutpat
                        dui. Morbi sed congue ligula, at pellentesque dui. Curabitur sed auctor magna. Aenean quis sem
                        efficitur, dapibus quam non, facilisis velit. Etiam feugiat tristique pretium.
                    </p>
                    <p>
                        Maecenas vel justo orci. Praesent sagittis efficitur erat quis vehicula. Etiam dignissim metus sit
                        amet purus tempus, nec egestas magna sollicitudin. Pellentesque vel eleifend diam. Duis auctor
                        tincidunt ornare. Quisque cursus vulputate quam in convallis. Donec ac maximus nibh. Fusce sed ante
                        et ligula euismod venenatis rutrum eu augue.
                    </p>
                    <p>
                        Aenean sit amet massa ac metus luctus laoreet at vel arcu. Donec vitae luctus magna, non elementum
                        nulla. Nullam fringilla elementum turpis. Cras varius, mauris ut consequat luctus, ex erat eleifend
                        turpis, id sodales quam sem in massa. Vestibulum rhoncus tellus massa, non pellentesque nisl
                        ullamcorper vel. Quisque fermentum, diam at scelerisque porta, velit sapien pretium metus, et
                        aliquam eros nisl sed libero. Suspendisse in risus quis sapien tempus semper ut sed metus. Curabitur
                        tincidunt ipsum non facilisis ultricies. Suspendisse sit amet lorem facilisis, laoreet eros
                        tincidunt, placerat felis. Curabitur nec nibh ac diam hendrerit porttitor. Duis vehicula metus
                        lorem, vel suscipit quam aliquet quis.
                    </p>';

        $page->translateOrNew('en')->first_block = $first_block;
        $page->translateOrNew('en')->second_block = $second_block;
        $page->save();
    }
}
