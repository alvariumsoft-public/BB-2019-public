<?php

use Illuminate\Database\Seeder;

class GpaAgreementPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = \App\Models\GpaAgreementPage::create([
            'image' => '/storage/img/s-top-bg-1.jpg',
            'slug' => 'gpa-agreement'
                                                     ]);
        $title = 'The fundamental aim of the GPA is to mutually open government procurement markets among its Parties. As a result of several rounds of negotiations, the GPA Parties have opened procurement activities worth an estimated US$ 1.7 trillion annually to international competition.';
        $page->translateOrNew('en')->title = $title;
        $page->save();
    }
}
