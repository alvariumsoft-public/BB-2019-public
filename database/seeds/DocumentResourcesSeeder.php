<?php

use Illuminate\Database\Seeder;

class DocumentResourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $document = \App\Models\DocumentResources::create([
//            'document' => '/storage/uploads/document_resources/13387 EBRD (GPA Facility Factsheet ARTWORK).pdf',
//                                              ]);
//        $document->translateOrNew('en')->title = 'Revised Agreement on Government Procurement';
//        $document->save();
//        $document = \App\Models\DocumentResources::create([
//                                                              'document' => '/storage/uploads/document_resources/13387 EBRD (GPA Facility Factsheet ARTWORK).pdf',
//                                                          ]);
//        $document->translateOrNew('en')->title = 'Gender in Public Procurement: women-led business in public procurement markets';
//        $document->save();
        \Illuminate\Support\Facades\DB::insert(file_get_contents(base_path('database/dumps/document-resources.sql')));
        \Illuminate\Support\Facades\DB::insert(file_get_contents(base_path('database/dumps/document-resources-translations.sql')));
    }
}
