<?php

use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $member = \App\Models\TeamMember::create([
//            'image' => 'storage/img/person-img-1.jpg'
//                                                 ]);
//        $detail = 'European Bank for Reconstruction and Development';
//        $description = '<p>
//                        Eliza Niewiadomska, Senior Counsel in the Legal Transition Programme of the European Bank for
//                        Reconstruction and Development in London (EBRD) is responsible for public procurement regulatory
//                        advice
//                        and technical cooperation within economies the EBRD operates. Key regional technical cooperation
//                        programmes led by Eliza include the EBRD UNCITRAL Public Procurement Reform Initiative, established
//                        in
//                        2011, the EBRD GPA Technical Cooperation Facility, formed in 2014 and Open Government, first
//                        initiated
//                        in 2014 to assist post-Maidan Revolution Ukraine.
//                    </p>
//                    <p>
//                        Lawyer and economist, Eliza worked for Polish government during regulatory reforms preparing
//                        Poland’s
//                        accession to European Union and upon leaving civil service, as a general counsel in the IT industry.
//                        Prior to joining the EBRD in 2009, Eliza was head of procurement at a power and energy company, PGE
//                        Polska Grupa Energetyczna SA in Warsaw.
//                    </p>';
//        $member->translateOrNew('en')->name = 'Eliza Niewiadomska';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'Senior Counsel';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-2.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Reto Malacrida';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'World Trade Organization';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-3.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Anna Caroline Müller';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'World Trade Organization';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-4.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Philippe Pelletier';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'World Trade Organization';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-5.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Jianning Chen';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'World Trade Organization';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-6.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Astghik Solomonyan';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'World Trade Organization';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-7.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Dr. Dorina Harcenco';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'EBRD GPA TC Facility';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-8.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Antonella Salgueiro';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'EBRD GPA TC Facility';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-9.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Eriks Mezalis';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'EBRD GPA TC Facility';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-10.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Andrei Rogac';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'EBRD GPA TC Facility';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-11.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Oleksandr Shatkovskyi';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'EBRD GPA TC Facility';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
//
//        $member = \App\Models\TeamMember::create([
//                                                     'image' => 'storage/img/person-img-12.jpg'
//                                                 ]);
//        $member->translateOrNew('en')->name = 'Taras Shymko';
//        $member->translateOrNew('en')->detail = $detail;
//        $member->translateOrNew('en')->position = 'EBRD GPA TC Facility';
//        $member->translateOrNew('en')->description = $description;
//        $member->save();
        \Illuminate\Support\Facades\DB::insert(file_get_contents(base_path('database/dumps/member.sql')));
        \Illuminate\Support\Facades\DB::insert(file_get_contents(base_path('database/dumps/member-translations.sql')));
    }
}
