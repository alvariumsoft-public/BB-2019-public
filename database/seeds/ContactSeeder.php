<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\ContactPage::create([
            'image' => '/storage/img/s-top-bg-7.jpg',
            'slug' => 'contacts'
                                        ]);
    }
}
