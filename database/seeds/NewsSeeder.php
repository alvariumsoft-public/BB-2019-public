<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\NewsPage::create([
            'image' => '/storage/img/s-top-bg-3.jpg',
            'slug' => 'news'
                                         ]);
//        for ($i = 1; $i <= 9; $i++){
//            $news = \App\Models\News::create([
//                                                 'image' => '/storage/img/news-img-3.jpg',
//                                                 'link' => '',
//                                                 'created_at' => '2020-10-07 09:00:0'.$i,
//                                                 'name' => 'news'.$i
//                                             ]);
//            $news->translateOrNew('en')->title = 'News Title '.$i;
//            $news->translateOrNew('en')->date = 'Q2 2014 – Q4 2015';
//            $news->translateOrNew('en')->description = '<p>Since June 2018, North Macedonia has received support in its bilateral negotiations with GPA Parties, including on presenting aceptable coverage offers. Related capacity building activities have also been provided.</p>';
//            $news->save();
//        }
//
//        $news = \App\Models\News::create([
//                                            'image' => '/storage/img/news-img-3.jpg',
//                                            'link' => '',
//                                            'created_at' => '2020-10-07 09:00:26',
//                                            'name' => 'Project Title'
//                                         ]);
//        $news->translateOrNew('en')->title = 'Project Title';
//        $news->translateOrNew('en')->date = 'Q2 2014 – Q4 2015';
//        $news->translateOrNew('en')->description = '<p>Since June 2018, North Macedonia has received support in its bilateral negotiations with GPA Parties, including on presenting aceptable coverage offers. Related capacity building activities have also been provided.</p>';
//        $news->save();
//
//        $news = \App\Models\News::create([
//                                             'image' => '/storage/img/news-img-2.jpg',
//                                             'link' => '',
//                                             'created_at' => '2020-10-07 09:00:27',
//                                             'name' => 'Project Title'
//                                         ]);
//        $news->translateOrNew('en')->title = 'Project Title';
//        $news->translateOrNew('en')->date = 'Q2 2014 – Q4 2015';
//        $news->translateOrNew('en')->description = '<p>The Facility supported the preparation of the national GPA negotiation strategy and provided legal and policy advice throughout the negotiation process.</p>';
//        $news->save();
//
//        $news = \App\Models\News::create([
//                                             'image' => '/storage/img/news-img-1.jpg',
//                                             'link' => '',
//                                             'created_at' => '2020-10-07 09:00:28',
//                                             'name' => 'Far from gender balance?'
//                                         ]);
//        $news->translateOrNew('en')->title = 'Far from gender balance?';
//        $news->translateOrNew('en')->date = '08 July 2020';
//        $news->translateOrNew('en')->description = '<p>The EBRD worked closely to facilitate GPA implementation and capacity building of domestic suppliers in Ukraine.</p>';
//        $news->save();
        \Illuminate\Support\Facades\DB::insert(file_get_contents(base_path('database/dumps/news.sql')));
        \Illuminate\Support\Facades\DB::insert(file_get_contents(base_path('database/dumps/news-translations.sql')));
        $news = \App\Models\News::all();
        foreach ($news as $item){
            $item->link = 'https://alvariumsoft.com/';
            $item->save();
        }
    }
}
