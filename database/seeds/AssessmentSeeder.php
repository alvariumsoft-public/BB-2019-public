<?php

use Illuminate\Database\Seeder;

class AssessmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = \App\Models\AssessmentsPage::create([
            'image' => '/storage/img/s-top-bg-2.jpg',
            'slug' => 'assessments'
                                            ]);
        $page->translateOrNew('en')->title = 'The revised World Trade Organization Agreement on Government Procurement (GPA), besides being recognized as a tool of good governance, has also taken a relevant role in guiding policy reforms particularly in transition economies.';
        $page->translateOrNew('en')->first_block = '<p>
                        Experience have shown that governments have sought or seek to join the GPA with the derivative aim
                        of improving governance and strengthening supplier competition in their own procurement markets
                        through better and more inclusive market access opportunities. Building up on the aforementioned,
                        and as a sign of the success of the GPA, its membership has grown significantly during the last
                        decade, including 7 new Parties and 19 Observers. Many of these Observers have already initiated
                        accession negotiations or have undertaken commitments to do so under their WTO accession package.
                    </p>';
        $page->translateOrNew('en')->second_block = '<p>
                        Under the work of the EBRD GPA TC Facility, EBRD consultants have been tasked to assess the primary
                        public procurement law (PPL) or existing draft legislation (draft PPL) of GPA Observers (under the
                        region of operation of the Bank) against a detailed questionnaire elaborated by the Bank’s experts
                        comprising the mandatory requirements of the GPA. The assessment seeks to identify possible
                        regulatory discrepancies with the text of the Agreement in order to facilitate further consultations
                        relating to accession negotiations. This exercise constitutes a preliminary analysis and does not
                        constitute under any circumstance a binding opinion. Notwithstanding, it provides, if applicable,
                        groundwork for the alignment of the Observer’s government procurement regime with the requirements
                        of the GPA, before or at the beginning of the accession process.
                    </p>';
        $page->save();
    }
}
