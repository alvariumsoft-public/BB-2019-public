<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGpaAgreementContentTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gpa_agreement_block_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();
            $table->unsignedBigInteger('gpa_agreement_block_id');
            $table->foreign('gpa_agreement_block_id')->references('id')->on('gpa_agreement_block')->onDelete('cascade');
            $table->string('title');
            $table->text('first_block');
            $table->text('second_block');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gpa_agreement_block_translations');
    }
}
