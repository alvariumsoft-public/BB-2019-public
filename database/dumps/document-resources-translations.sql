INSERT IGNORE INTO `document_resources_translations` (`id`, `locale`, `document_resources_id`, `title`) VALUES
(1, 'en', 1, 'Revised Agreement on Government Procurement'),
(2, 'en', 2, 'Gender in Public Procurement: women-led business in public procurement markets');
