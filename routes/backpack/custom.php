<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('page', 'PageCrudController');
    Route::crud('post', 'PostCrudController');
    Route::crud('news', 'NewsCrudController');
    Route::crud('teammember', 'TeamMemberCrudController');
    Route::crud('homepage', 'HomePageCrudController');
    Route::crud('gpaagreementpage', 'GpaAgreementPageCrudController');
    Route::crud('technicaloperation', 'TechnicalOperationCrudController');
    Route::crud('assessmentspage', 'AssessmentsPageCrudController');
    Route::crud('resourcespage', 'ResourcesPageCrudController');
    Route::crud('contactpage', 'ContactPageCrudController');
    Route::crud('termspage', 'TermsPageCrudController');
    Route::crud('privacypolicypage', 'PrivacyPolicyPageCrudController');
    Route::crud('taskforcepage', 'TaskforcePageCrudController');
    Route::crud('gpaagreementcontent', 'GpaAgreementContentCrudController');
    Route::crud('newspage', 'NewsPageCrudController');
    Route::crud('documentresources', 'DocumentResourcesCrudController');
}); // this should be the absolute last line of this file
