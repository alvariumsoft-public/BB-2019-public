<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/set-locale/{locale}', 'LanguageController@setLocale')->name('setLocale');

Route::group(['middleware'=>'language'],function () {
    Auth::routes();

    Route::get('/markup/{template}', function ($template) {
        return view("/markup/$template");
    });
    Route::get('/', 'Front\HomePageController@index')->name('home');
    Route::get('/gpa-agreement', 'Front\PageController@gpa')->name('gpa');
    Route::get('/assessments', 'Front\PageController@assessments')->name('assessments');
    Route::get('/resources', 'Front\PageController@resources')->name('resources');
    Route::get('/technical-cooperation', 'Front\PageController@cooperation')->name('cooperation');
    Route::get('/technical-cooperation/{post}', 'Front\PageController@posts')->name('post');
    Route::get('/news', 'Front\PageController@news')->name('news');
    Route::get('/privacy-policy', 'Front\PageController@privacy')->name('privacy');
    Route::get('/contacts', 'Front\PageController@contacts')->name('contacts');
    Route::get('/terms-and-conditions', 'Front\PageController@terms')->name('terms');
    Route::get('/taskforce', 'Front\PageController@taskforce')->name('taskforce');
    });
